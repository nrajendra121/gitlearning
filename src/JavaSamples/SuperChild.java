package JavaSamples;

public class SuperChild extends SuperParent {
	
	
	
	public SuperChild()
	{
	super();//"Super" keyword should always be given in first line
	System.out.println(" Child class constructor");
	}
	
	String name = "Rajendra Neerukonda";
	
	
	public void getStringdata(){
		System.out.println(name);
		System.out.println(super.name);
}

	
	public void getdata()
	{
		super.getdata();
		System.out.println(" i am the child class");
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		SuperChild sc=new SuperChild();
		sc.getStringdata();
		sc.getdata();
	}

}
