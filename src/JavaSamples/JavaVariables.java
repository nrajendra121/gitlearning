package JavaSamples;

public class JavaVariables {
  public static void main (String [] args){
	  //variable declaration
	  int a;
	  a = 100;//initialization
	  //declaration of multiple variables in the statement
	  int b, c, d;
		  b = 20;
		  c = 10;
		  d = 20;
		  //declaration of multiple variable and assigning values
		  int e = 50, f = 60, g = 70;
		  char x= 'A', n= 'b';
		  double y = 22.345;
	      String zx = "selenium123";
	      System.out.println(a);
	      System.out.println(b);
	      System.out.println(c);
	      System.out.println(d);
	      System.out.println(e);
	      System.out.println(f);
  }
}
