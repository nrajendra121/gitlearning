package JavaSamples;

public class strings {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Java String Example
		System.out.println("133$%selenium");
		System.out.println("how r u 123");
	
		
		
		//Example for Creating Strings
		
		String myTool ="selenium";
		String [] myTools= {"UFT", "selenium", "loadrunner"};//Array of strings
		
		System.out.println(myTool);
		
		for(int i = 0;i<myTools.length;i++){
			System.out.println(myTools[i]);	
		}
		
		
		//Concatenating Strings
		String str1 = "Selenium"; // String Variable
	    String str2 = "Testing";

	    System.out.println(str1 + str2);// Selenium Testing
	    System.out.println("Test Automation " + "Using Selenium" + " and Java"); // Test Automation using Selenium and java
	    System.out.println(1 + 1 + " Selenium");
	    System.out.println("Selenium" +1 + 1);
	    
	    
	
	    //String Comparison in Java...........
	    String str3 = "SELENIUM"; 
	    String str4 = "selenium";
	    String str5 = "SELENIUM";
	    String str6 = "selenium testing";
	    
	    System.out.println(str3.equals(str4));// false
	    System.out.println(str3.equals(str5));// true
	    
	    System.out.println(str3 == str4); // false
	    System.out.println(str3 == str5); // true
	    
	    
	    
	    //Important operations on Strings
	    
	    String str7 = "Selenium";
	       String str8 ="Rajendra neerukonda";
	       
	       System.out.println(str7.length());//  (finding length of the String)
	      
	       System.out.println(str7.contains("len"));// true (finding sub string)
	       System.out.println(str7.contains("lem"));// false (finding sub string)
	      
	      // Returning Sub Strings
	       System.out.println(str8.substring(0)); 
	       System.out.println(str8.substring(9)); 
	       System.out.println(str8.substring(14)); 
	       System.out.println(str8.substring(9, 13)); 
	       System.out.println(str8.charAt(7));
	       System.out.println(str8.indexOf("n"));
	     
	       
	       
	}

}
