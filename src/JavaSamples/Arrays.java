package JavaSamples;

public class Arrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//Declaration of Arrays in Java:
			int abc []; // Array of Integers
            abc = new int[4]; //creating Array and defining size.
            System.out.println(abc.length); //Finding length of the Array.

			abc[0] =10;
			abc[1] = 20;
          System.out.println(abc[0]+abc[1]);
          
         
          //creating array with size
          int [] xyz = new int[5];

          xyz[0] =2;
          xyz[1] =3;
          System.out.println(xyz.length);
          System.out.println(xyz[0]+xyz[1]);
          
       
          
          
          
          //Creating Array and Initializing
	
          int [] uvw = {10, 20, 30, 40, 50}; 
          System.out.println(uvw.length);
          System.out.println(uvw[1]+uvw[2]);
          //Printing Array
          for (int i=0; i < uvw.length; i++) {
              System.out.println(uvw[i]);    
              }
          
          
          //Creating Arrays (Different data types)
          int [] array1 ={1, 2, 3, 4, 5}; // Array of Integers
          char [] array2 ={'A', 'B', 'C'}; // Array of Characters
          boolean [] array3 = {true, false, false, true,false}; // Array of Boolean values
          String [] array4 = {"Selenium", "UFT", "Java", "LoadRunner"}; // Array of Strings
              
              System.out.println(array1[1]); // 2
              System.out.println(array2[1]); // B
              System.out.println(array3[1]); // false
              System.out.println(array4[0]); // Selenium
              
              
              //Copy of values an Array into another Array
              int[] array5 = {1, 2, 3, 4, 5,6};
              int[]array6=array5;
              System.out.println(array6[2]);
              
              for(int i=0;i<array6.length;i++){
            	  System.out.println(i);
              }
	}

}
