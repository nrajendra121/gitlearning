package JavaSamples;

public class Operators2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Arithmetic Operators
		int a = 10, b = 5;
		String c = "selenium", d = "Testing";
		
		System.out.println("Addition of a, b is" +(a+b));
		System.out.println("Concatination of c, d is " +(c+d));
		 
		System.out.println("Subtraction if a, b is " +(a-b));
		
		System.out.println("Multiplication of a, b is " +(a*b));
		
		System.out.println(" division of a, b is" +(a/b));
		
		System.out.println("Modulas of a, b is" +(a%b));
		
		a = ++b;
		System.out.println(a);//6
		b = 5;
		a = --b;
		System.out.println(a);//4
		b = 5;
		a = b+4;
		System.out.println(a);//9
		b = 5;
		a = b-4;
		System.out.println(a);//1
	}

}
