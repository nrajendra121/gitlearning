package JavaSamples;

public class Inheritance1 {
	
	static int a=10, b= 20;             //Static variables
	int c=7, d=8;                       //non static variables
	
     public static void add(){           //Static method
	 System.out.println(" Addition of a and b is "+(a+b));
 }

 public void add2(){                    //Non static method
	System.out.println("addition of c and d is"+(c+d));
 }
 
 public static void main (String [] args){
	                                   //Access static class members using class name
	 System.out.println(Inheritance1.a);
	 Inheritance1.add();
 
                                        //Access non static class members using object
 Inheritance1 object=new Inheritance1();
 System.out.println(object.c);
 object.add2();
 
}
}