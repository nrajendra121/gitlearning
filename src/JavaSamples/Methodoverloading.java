package JavaSamples;

public class Methodoverloading {
	
	public static int add(int a, int b){
		int result;
		result = a+b;
		return result;
	}
	
	/*public static int add(int a, int b, int c){
		int result;
		result = a+b+c;
		return result;
	}*/
	
	//By changing the data type name
	
	public static double add(double a, double b, double c){
		double result;
		result = a+b+c;
		return result;
	}
	
	public static void main(String [] args){
		int x = add(10,20);
		double y= add(20.5415,32.565,66.212);
		System.out.println(x);
		System.out.println(y);
	}

	
	
}
