package JavaSamples;

public class polymorphism1 {                 //Compile time polymorphism. number of Arguments,order of Arguments, type of Arguments
	
	public void add (int a, int b){
        System.out.println(a+b);
    }
	
	
    public void add (int a, int b, int c){
        System.out.println(a+b+c);
    }
    
    
        public void add (double a, double b){
            System.out.println(a+b);    
    }
    public static void main (String [] args){
    	polymorphism1 obj = new polymorphism1();
        obj.add(2, 5);
        obj.add(2, 5, 7);
        obj.add(1.234, 4.567);
    }
}


