package JavaSamples;

public class Loops {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	//print 1 to 10 numbers using for loop.It repeats a block of statements for a specified number of times.
	for (int i=1;i<=10;i++){
		System.out.println(i);
	}
	
	//print 10 to 1 numbers
	for(int i=10;i>=1;i--){
		if ((i!=4)&&(i!=7)){
		System.out.println(i);
	}
	}
	// print every 10th Number up to 100
	
	for (int i=10;i<=100;i=i+10){
		System.out.println(i);
	}
	
	
	
	//Print 1 to 10 using  while loop
	/*int i = 1;
	while (i<=10){
		System.out.println(i);
		i++;
	}*/
	//Print 10 to 1 using  while loop.It repeats a block of statements while condition is true.
	int i=10;
	while(i>=1){
		System.out.println(i);
		i--;
	}
	
	//do while Loop
	//it repeats a block of statements while condition is true and Irrespective of the condition, it executes a block of statements at least once.
	int v = 20;
    do {
     System.out.println("v value is " + v);
     v++;
    } while(v <= 10);
    
    
    //Enhanced for Loop.It executes all elements in an Array
    String languages [] = {"C", "C++", "Java", "COBOL"};
    for (String lang: languages)
    {
        System.out.println(lang);
    }
	}

}
