package JavaSamples;

public class Operators3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
         
		//Relational Operators
		
		 int a = 10, b = 20;
		    System.out.println("a > b is " + (a>b)); 
		    System.out.println("a >= b is " + (a >= b)); 
		    
		    System.out.println("a < b is " + (a<b)); 
		    System.out.println("a <= b is " + (a <= b)); 
		    
		    System.out.println("a == b is " + (a == b)); 
		    System.out.println("a != b is " + (a != b)); 
	}

}
