package JavaSamples;

public class Conditionalstatements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  //Executing a block of statement when the condition is true
		int a, b;
		a = 10;
		b = 5;
		if (a>b){
			System.out.println(" A is a big number");
		}
	
		//Executing a block of statement when Compound condition is true
       int c, d, e;
		c = 10;
		d = 5;
		e = 3;
		 if ((c>d)&&(c>e)){
			 System.out.println(" A is a big number");
			 
		 }
		 
		 //Executing a block of statement when the condition is true. otherwise executing another block of statements
		 int x, y;
		 x = 10;
		 y = 20;
		 if (x>y){
			 System.out.println(" X is big  number");
		 }
		 else {
			 System.out.println("y is a big number");
		 }
		 
		 
		//Decide among the several alternatives(else if structure)
		 
		 int z = -100;
		 
		 if ((z >= 1) && (z < 100)) {
			 System.out.println("z is a small number");
		 }
		 
		 else if ((z>100) && (z<=1000))	{
			 System.out.println("z is a medium number");
		 }
			 
		 else if (z > 1000){
			 System.out.println("z is a big number");
		 }
		 
		 else{
			 System.out.println( "z is zero or negative value");
		 }
		 
		 
		 // Executing a block of statements when more than one condition is true (Nested if).
		 int u =10, v =7, n = 5, m = 13;
         
	        if (u > v){}
	          if (u > n) {}
	           if (u > m){
	               System.out.println("u is a Big Number");
	           }
	           else {
	               System.out.println("u is Not a Big Number");
	           }
		 }
		 }
		

